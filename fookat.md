**Which LXC images are good for VNC?** I've had varying success with installing XFCE and RealVNC server on different LXC images, so I tested each one to figure out which ones I should use.

- Host: Ubuntu 20.04.1 LTS on kernel Linux 5.4.0-52-generic
- LXD version 4.7



## Ubuntu 20.04

### Setup procedure
```bash
lxc launch ubuntu:20.04 test
lxc exec test -- bash
apt update
apt install xfce4
wget https://www.realvnc.com/download/file/vnc.files/VNC-Server-6.7.2-Linux-x64.deb
apt install ./VNC-Server-6.7.2-Linux-x64.deb
vnclicense -add 4R22B-2BA48-RZH9Y-MLAAF-HCYNA
su -l ubuntu
vncserver :1
```

### Result
VNC session immediately exits. Here are the contents of the [VNC log](https://gist.github.com/ledlamp/d32d14cf7bb25055028a180a704ec7c1) and [.xsession-errors](https://gist.github.com/ledlamp/724d32a70eff40edfbd733f25cb5a6f1).

Enabling system Xorg did not help.




## Debian 10

### Setup procedure
```bash
lxc launch images:debian/10 test2
lxc exec test2 -- bash
apt update
apt install xfce4
apt install wget
wget https://www.realvnc.com/download/file/vnc.files/VNC-Server-6.7.2-Linux-x64.deb
apt install ./VNC-Server-6.7.2-Linux-x64.deb
vnclicense -add 4R22B-2BA48-RZH9Y-MLAAF-HCYNA
vncserver :1
# this works, so let's finish set up
echo "ConnectToExisting=1" >> /etc/vnc/config.d/vncserver-virtuald
systemctl enable --now vncserver-virtuald
adduser foo
```

### Result
[VNC session works nicely.](https://3t32.qonq.gq)

However due to the old Xorg server built into Xvnc, you cannot change the resolution using the Display settings, so you have to add i.e. `-geometry 1366x768` to your `~/.vnc/config`. And for some reason, RealVNC does not recommend using the system Xorg server on Debian/Ubuntu systems.

But I did it anyway and it works.

![image](https://yowt.qonq.gq)



## Fedora 33

### Setup procedure
```bash
lxc launch images:fedora/33 test3
lxc exec test3 -- bash
dnf groupinstall Xfce
dnf install https://www.realvnc.com/download/file/vnc.files/VNC-Server-6.7.2-Linux-x64.rpm
vnclicense -add 4R22B-2BA48-RZH9Y-MLAAF-HCYNA
vncserver :1
dnf install xorg-x11-drv-dummy
vncinitconfig -enable-system-xorg
# Now it starts, but...
```

### Result
Session immediately exits. 
[[VNC log]](https://gist.github.com/ledlamp/69ab733c29010dad7092e912795f9ebc)
[[.xsession-errors]](https://gist.github.com/ledlamp/e267ced684a43fe0c81a4488df6fa0d4)



## CentOS 8

### Setup procedure
```bash
lxc launch images:centos/8 test4
lxc exec test4 -- bash
dnf install epel-release
dnf groupinstall Xfce
dnf install https://www.realvnc.com/download/file/vnc.files/VNC-Server-6.7.2-Linux-x64.rpm
vnclicense -add 4R22B-2BA48-RZH9Y-MLAAF-HCYNA
dnf install xorg-x11-drv-dummy
vncinitconfig -enable-system-xorg
vncserver :1
```

### Result
This works, but for some reason the Xfce group on CentOS includes a [minimal GNOME session](https://umok.qonq.gq), which the xstartup script will run instead of Xfce. So the [xstartup script has to be customized to explicitly start Xfce.](https://help.realvnc.com/hc/en-us/articles/360003474792-Why-does-VNC-Server-in-Virtual-Mode-on-Linux-appear-to-hang-show-a-gray-screen-or-not-start-at-all-#using-vnc-5-x-or-vnc-connect-6-0-6-1-or-on-a-different-version-of-linux--0-1)

So after adding the following to /etc/vnc/xstartup.custom and making it executable...
```sh
#!/bin/sh
DESKTOP_SESSION=xfce
export DESKTOP_SESSION
startxfce4
vncserver-virtual -kill $DISPLAY
```
[The Xfce session works now.](https://hopc.qonq.gq) And the resolution can be changed in Display settings.

![image](https://0uqh.qonq.gq)

### Conclusion

CentOS does not seem like a good choice for a virtual XFCE desktop, but it works well with GNOME. You can install the 'Server with GUI' group for the complete experience.



## OpenSUSE 15.2
```bash
lxc launch images:opensuse/15.2 test5
lxc exec test5 -- bash
zypper install -t pattern xfce
zypper install https://www.realvnc.com/download/file/vnc.files/VNC-Server-6.7.2-Linux-x64.rpm
zypper install xauth
vnclicense -add 4R22B-2BA48-RZH9Y-MLAAF-HCYNA
vncserver :1
# xstartup did not find XFCE, so I had to create a custom xstartup like I did for CentOS
vncserver -kill :1
zypper install nano
nano /etc/vnc/xstartup.custom # pasted the stuff
chmod a+x /etc/vnc/xstartup.custom
vncserver :1
passwd
```

[And it works :)](https://r14r.qonq.gq)

However, like Debian, it uses Xvnc so changing resolution is a little tedious, and they do not recommend using system Xorg on SUSE.

But I tried it anyway (`zypper install xf86-video-dummy; vncinitconfig -enable-system-xorg`) and it works!

![image](https://vqq7.qonq.gq)



## Fedora 32
```bash
lxc launch images:fedora/32 test6
lxc exec test6 -- bash
dnf groupinstall Xfce
dnf install https://www.realvnc.com/download/file/vnc.files/VNC-Server-6.7.2-Linux-x64.rpm
vnclicense -add 4R22B-2BA48-RZH9Y-MLAAF-HCYNA
dnf install xorg-x11-drv-dummy
vncinitconfig -enable-system-xorg
vncserver :1
```

VNC server immediately exits. [[VNC log]](https://gist.github.com/ledlamp/9564bea71f132f62394d7c012d7a1147) [[.xsession-errors]](https://gist.github.com/ledlamp/bda981fca7f5778f2aaf6834260a0a98)

This is weird, because I had previously set up a Fedora 32 container with Xfce VNC just like this and it worked fine. But when I tried with Fedora 33, I had this problem, so this is why I tested Fedora 32 again.

Could it maybe be because I had installed the "Development Tools" group before installing Xfce?

I tried installing it here but still have the same issue. Weird.

Maybe it's because that container was on a CentOS 8 kernel (4.18.0-193.19.1.el8_2.x86_64), while this one is on Ubuntu kernel (5.4.0-52-generic)? 🤷




## Ubuntu 18.04
```bash
lxc launch ubuntu:18.04 test7
lxc exec test7 -- bash
apt update
apt install xfce4
wget https://www.realvnc.com/download/file/vnc.files/VNC-Server-6.7.2-Linux-x64.deb
apt install ./VNC-Server-6.7.2-Linux-x64.deb
vnclicense -add 4R22B-2BA48-RZH9Y-MLAAF-HCYNA
passwd ubuntu
su -l ubuntu
vncserver :1
```
[It works!](https://ubuh.qonq.gq)

And it also works with system Xorg! (`apt install xserver-xorg-video-dummy; vncinitconfig -enable-system-xorg`)

![image](https://o3os.qonq.gq)


## Ubuntu 16.04
```bash
lxc launch ubuntu:16.04 test8
lxc exec test8 -- bash
apt update
apt install xfce4
wget https://www.realvnc.com/download/file/vnc.files/VNC-Server-6.7.2-Linux-x64.deb
apt install ./VNC-Server-6.7.2-Linux-x64.deb
vnclicense -add 4R22B-2BA48-RZH9Y-MLAAF-HCYNA
su -l ubuntu
vncserver :1

```

Xenial is nearing end of life soon, but I was curious. [It also works fine :)](https://d6iw.qonq.gq)




## Linux Mint 19 (based on Ubuntu 18.04)
```
lxc launch images:mint/tricia test9
lxc exec test9 -- bash
apt install xfce4
apt install wget
wget https://www.realvnc.com/download/file/vnc.files/VNC-Server-6.7.2-Linux-x64.deb
apt install ./VNC-Server-6.7.2-Linux-x64.deb
vnclicense -add 4R22B-2BA48-RZH9Y-MLAAF-HCYNA
su -l ubuntu
vncserver :1
```

Huh, this does not work. [[VNC log]](https://gist.github.com/ledlamp/03a92d55a483398006456f31a4b89417) [[.xsession-errors]](https://gist.github.com/ledlamp/fdfa1e8e9e3c877616e06c78747c61d8)

Not testing Mint 20 as I'm pretty sure it won't work. Thought this might as it's based on 18.04.



## Fedora 33, but with GNOME
```
lxc launch images:fedora/33 test10
lxc exec test10 -- bash
dnf groupinstall GNOME
dnf install https://www.realvnc.com/download/file/vnc.files/VNC-Server-6.7.2-Linux-x64.rpm
vnclicense -add 4R22B-2BA48-RZH9Y-MLAAF-HCYNA
dnf install xorg-x11-drv-dummy
vncinitconfig -enable-system-xorg
vncserver :1
```

It works!
https://o1tc.qonq.gq 

![image](https://2gw7.qonq.gq)




# Conclusion

Preferably, you want to [use the system Xorg](https://help.realvnc.com/hc/en-us/articles/360003474752-How-do-I-use-VNC-Server-in-Virtual-Mode-on-Linux-in-conjunction-with-the-system-Xorg-server-) so you can change display resolution and run some newer software that requires it. But RealVNC only recommends (and in my experience, requires) using it on RedHat-based distros, not on other distros for some reason. But you can try it anyway and it seems to work fine, so I don't know why they advise against it.

So if you want to follow their recommendation, that leaves you with just CentOS or Fedora, which haven't given me the greatest experience with setting up Xfce VNC, the latter not even working at all, which is weird, because it worked nicely on my other system, so I can only guess it's because of the different kernels. (same LXD version btw)

However, both of these distros work great if you want GNOME. It's a nice desktop environment but relatively resource intensive so I don't recommend it for small or underpowered systems. For those, you want to use XFCE. But if you have a fairly beefy host then CentOS with GNOME might be the ideal VNC platform. Or you can install the "Server with GUI" package group for even more goodies.

But since system Xorg seems to work fine on other distros, Debian 10 and Ubuntu 18.04 look like the ideal choices. I could never get Xfce VNC to work in 20.04 containers and could never figure out why. But at least 18.04 works, which is pretty similar to 20.04 and still has a few years of support left. Ubuntu has more stuff pre-installed, including snapd (with lxd pre-installed) and SSH server (with password authentication disabled tho), and a pre-made `ubuntu` user you can use right away. While Debian is slimmer but may need a little more set up.

openSUSE 15.2 is also a fairly nice option; you just need to add the custom xstartup config, but it has a nice openSUSE-themed pre-configured Xfce desktop with whisker menu and task bar on the bottom.

Note that this isn't a tutorial, but if you're installing VNC, you should create a user (don't use root), enable and use vncserver-virtuald, and uninstall any screensavers. You don't want xscreensaver wasting cpu on drawing a nice animation that nobody sees.